import "./Test.css";
import React from "react";
import Item from '../Item/index'
function template() {
  const {data}=this.state;
  return (
    <div className="test">
       {
         data.map((val,index)=>{
            return <Item key={index} data={val} />
         })
       }
    </div>
  );
};

export default template;
