import "./Item.css";
import React from "react";

function template() {
  const {data}=this.props;
  const {isDisableBtn,doi} =this.state;
  const disableClass=isDisableBtn ? 'opacity-p2' : '';
  return (
    <div className="item">
        <div>
          {data} <span className="doi">{doi}</span>

        </div>
        <div>
        <input disabled={isDisableBtn} className={`${disableClass} crosscheck-btn` } onClick={this.fnItemClick.bind(this,data)} type='button' value='CrossRef Check' />
          </div> 
    </div>
  );
};

export default template;
