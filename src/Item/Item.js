import React    from "react";
import template from "./Item.jsx";
import ServerCall from '../Services/ServerCall';
class Item extends React.Component {
  constructor(){
    super();
    this.state={
      isDisableBtn:false,
      doi:''
    }
  }
  render() {
    return template.call(this);
  }

  fnItemClick(data){
     ServerCall.fnGetReq('https://api.crossref.org/works?sort=score&order=desc&rows=1&query.bibliographic='+data)
     .then((res)=>{
        if(res.data.message.items.length){
          let item=res.data.message.items[0];
          let doi=item.DOI;
          this.setState({
             isDisableBtn:true,
             doi:'DOI:'+doi
          })
        }
     })
     .catch(()=>{
       debugger;
     })
  }
}

export default Item;
